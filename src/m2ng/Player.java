package m2ng;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

public class Player {

	public static final int MAX_V = 100;
	public static final int MAX_TOP = 50;
	public static final int MAX_BOTTOM = 480;

	Image imgcentr = new ImageIcon("res/mashinaedet.png").getImage();
	Image imgleft = new ImageIcon("res/mashinavpravo.png").getImage();
	Image imgright = new ImageIcon("res/mashinavlevo.png").getImage();

	Image img = imgcentr;

	public Rectangle getREct() {
		return new Rectangle(x, y, 160, 56);
	}

	int v = 0;
	int dv = 0;
	int s = 0;

	int x = 30;
	int y = 100;
	int dy = 0;

	int layer1 = 0;
	int layer2 = 1071;

	public void move() {
		s += v;
		v += dv;
		if (v <= 0)
			v = 0;
		if (v >= MAX_V)
			v = MAX_V;

		y -= dy;
		if (y <= MAX_TOP)
			y = MAX_TOP;
		if (y >= MAX_BOTTOM)
			y = MAX_BOTTOM;
		if (layer2 - v <= 0) {
			layer1 = 0;
			layer2 = 1100;
		} else {

			layer1 -= v;
			layer2 -= v;

		}
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_RIGHT) {
			dv = 5;
		}
		if (key == KeyEvent.VK_LEFT) {
			dv = -5;
		}
		if (key == KeyEvent.VK_UP) {
			dy = 10;
			img = imgleft;
		}
		if (key == KeyEvent.VK_DOWN) {
			dy = -10;
			img = imgright;
		}

	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT) {
			dv = 0;
		}
		if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
			dy = 0;
			img = imgcentr;

		}
	}
}